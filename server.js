var http = require('http');
var fs = require('fs');
var path = require('path');
var socketio = require("socket.io");
var MongoClient = require('mongodb').MongoClient;
var connectError = false;
var ready = [];
var gwint;

/*-----------------------HTTP--------------------------*/
var httpServer = http.createServer(function(req, res) {
  switch (req.method) {
    case "GET":
      if (req.url == "/") req.url = "/index.html";
      if (req.url != "/favicon.ico") {
        req.url = req.url.replace(/%20/g, " ");
        var filename = path.join(__dirname, req.url);
        console.log(req.url);
        var fileStream = fs.createReadStream(filename);
        fileStream.on('data', function(data) {
          res.write(data);
        });
        fileStream.on('end', function() {
          res.end();
        });
      }
      break;
  }
}).listen(3000);

/*-----------------------SOCKET--------------------------*/
var io = socketio.listen(httpServer); //Ciebie interesują tylko Sockety

io.sockets.on("connection", function(client) { //Przy połączeniu do serwera wykonywany jest ten event
  client.emit("onconnect", { //odesłanie unikalnego id klienta
    clientName: client.id
  });

  client.on("drawCards", function(data) { //klient upomina się o 10 kart z bazy
    gwint.collection('karty').find({
      $or: [{
        fraction: data.deck,
        "strength": {
          $gte: 0
        }
      }, {
        fraction: "Neutral"
      }, {
        fraction: "Special"
      }]
    }).toArray(function(err, items) {
      client.emit("10Cards", { //wysłanie do klienta tablicy z 10 kartami
        deck: JSON.stringify(items)
      });
    });
  });

  client.on("endTurn", function(data) { //serwer dostaje wiadomość gracz zakonczył ture
    io.sockets.emit("changeTurn", {}); //rozesłanie na broadcascie wymuszenia zmiany tury
    client.broadcast.emit("updatePlayField", { //update playfieldu u przeciwnika
      card: data
    });
  });

  client.on("musted", function(data) { //serwer dostaje wiadomość gracz zakonczył ture
    client.broadcast.emit("musted", { //update playfieldu u przeciwnika
      card: data.card
    });
  });

  client.on("jaskier", function() { //serwer dostaje wiadomość gracz zakonczył ture
    client.broadcast.emit("jaskier", {}); //update playfieldu u przeciwnika
  });

  client.on("surrender", function() { //serwer dostaje wiadomość gracz zakonczył ture
    client.broadcast.emit("surrender", {}); //update playfieldu u przeciwnika
  });

  client.on("hand", function(data) { //serwer dostaje wiadomość gracz zakonczył ture
    client.broadcast.emit("hand", {
      hand: data.hand
    }); //update playfieldu u przeciwnika
  });

  client.on("morale", function() { //serwer dostaje wiadomość gracz zakonczył ture
    client.broadcast.emit("morale", {}); //update playfieldu u przeciwnika
  });

  client.on("ready", function(data) { //kiedy gracz jest gotowy serwer dopisuje go do tablicy
    ready.push({ //o tutaj
      id: client.id,
      deck: data.deck,
      champion: data.champion
    });
    if (ready.length == 2) { //jejest 2 graczy to mozacząć gre
      var array = JSON.stringify(ready);
      for (var x in ready) {
        var first = false;
        if (x == 0)
          first = true;
        io.sockets.connected[ready[x].id].emit('moveToPlayfield', { //rozesłanie do poszczeglnych klientw danych umorozpoczęscie
          player: true,
          enemy: array, //tablica z kartami
          turn: first, //kto zaczyna
        });
      }
    }
  });
});

/*-----------------------MONGODB--------------------------*/
MongoClient.connect("mongodb://localhost/gwint", function(err, db) {
  if (err) {
    connectError = true;
    return console.dir(err);
  }
  gwint = db;
  db.collection('karty', {
    strict: true
  }, function(err, coll) {
    if (err !== null) {
      console.log("Kolekcja nie istnieje");
      var collection = db.collection('karty');
      collection.insert(karty);
    } else {
      console.log("kolekcja istnieje");
    }
  });
});


/*-----------------------KARTY KURWA--------------------------*/
var karty = [{
    "name": "Foltest the Siegemaster",
    "fraction": "Temeria",
    "ability": "Doubles the strength of all Siege units, unless a Commander's Horn is already in play on that row."
  }, {
    "name": "Foltest the Steel-Forged",
    "fraction": "Temeria",
    "ability": "Destroy your enemy's strongest Siege unit(s) if the combined strength of all his or her Siege units is 10 or more."
  }, {
    "name": "Foltest King of Temeria",
    "fraction": "Temeria",
    "ability": "Pick an Impenetrable Fog from your deck and play it instantly."
  }, {
    "name": "Foltest Lord Commander of the North",
    "fraction": "Temeria",
    "ability": "Clear any weather effects (on both sides) currently in play."
  }, {
    "name": "Eredin Commander of the Red Riders",
    "fraction": "Monsters",
    "ability": "Pick any weather card from your deck and play it instantly."
  }, {
    "name": "Eredin Bringer of Death",
    "fraction": "Monsters",
    "ability": "Discard 2 cards and draw 1 card of your choice from your deck."
  }, {
    "name": "Eredin Destroyer of Worlds",
    "fraction": "Monsters",
    "ability": "Restore a card from your discard pile to your hand."
  }, {
    "name": "Eredin King if the Wild Hunt",
    "fraction": "Monsters",
    "ability": "Double the strength of all your Close Combat units (unless a Commander's Horn is also present on that row)."
  }, {
    "name": "Torrential Rain",
    "fraction": "Special",
    "type": "Weather",
    "ability": "Sets the strength of all Siege Combat cards to 1 for both players.",
    "quantity": 2
  }, {
    "name": "Impenetrable Fog",
    "fraction": "Special",
    "type": "Weather",
    "ability": "Sets the strength of all Ranged Combat cards to 1 for both players.",
    "quantity": 2
  }, {
    "name": "Bitting Frost",
    "fraction": "Special",
    "type": "Weather",
    "ability": "Sets the strength of all Close Combat cards to 1 for both players.",
    "quantity": 2
  },
  //  {
  //   "name": "Commander's Horn",
  //   "fraction": "Special",
  //   "type": "Horn",
  //   "ability": "Doubles the strength of all unit cards in that row. Limited to 1 per row.",
  //   "quantity": 2
  // },
  {
    "name": "Clear Weather",
    "fraction": "Special",
    "type": "Weather",
    "ability": "Removes all Weather Card (Biting Frost, Impenetrable Fog, and Torrential Rain) effects.",
    "quantity": 2
  }, {
    "name": "Avallac'h",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 0,
    "ability": "Hero: Not affected by Special Cards or abilities.|Spy: Place on your opponent's battlefield (counts towards opponent's total) and draw 2 cards from your deck.",
    "hero": true
  }, {
    "name": "Cirilla Fiona Elen Riannon",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 15,
    "ability": "Hero: Not affected by Special Cards or abilities.",
    "hero": true
  }, {
    "name": "Geralt of Rivia",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 15,
    "ability": "Hero: Not affected by Special Cards or abilities.",
    "hero": true
  }, {
    "name": "Dandelion",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 2,
    "ability": "Commander's Horn : Doubles the strength of all units in that row. Limited to 1 per row.",
    "hero": false
  }, {
    "name": "Villentretenmerth",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 7,
    "ability": "Scorch - Close Combat: Destroy your enemy's strongest Close Combat unit(s) if the combined strength of all his or her Close Combat units is 10 or more.",
    "hero": false
  }, {
    "name": "Triss Merigold",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 7,
    "ability": "Hero: Not affected by Special Cards or abilities.",
    "hero": true
  }, {
    "name": "Vesemir",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 6,
    "ability": "",
    "hero": false
  }, {
    "name": "Yennefer of Vengerberg",
    "fraction": "Neutral",
    "type": "Ranged Combat",
    "strength": 7,
    "ability": "Hero: Not affected by any Special Cards or abilities.|Medic: Choose one card from your discard pile and play it instantly (no Heroes or Special Cards).",
    "hero": true
  }, {
    "name": "Zoltan Chivay",
    "fraction": "Neutral",
    "type": "Close Combat",
    "strength": 5,
    "ability": "",
    "hero": false
  }, {
    "name": "Ballista",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "",
    "strength": 6,
    "quantity": 2,
    "hero": false
  }, {
    "name": "Dethmold",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Sigismund Dijkstra",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Spy: Place on your opponent's battlefield (counts towards opponent's total) and draw 2 cards from your deck.",
    "strength": 4,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Kedweni Siege Expert",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "Morale Boost: Adds +1 to all units in the row.",
    "strength": 1,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Catapult",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "Tight Bond: Place next to a card with the same name to double the strength of both cards.",
    "strength": 8,
    "quantity": 2,
    "hero": false
  }, {
    "name": "Keira Metz",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Crinfrid Reavers Dragon Hunter",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "Tight Bond: Place next to a card with the same name to double the strength of both cards.",
    "strength": 5,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Dun Banner Medic",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "Medic: Choose one card from your discard pile and play it instantly (no Heroes or Special Cards).",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "John Natalis",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Blue Stripes Commando",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Tight Bond: Place next to a card with the same name to double the strength of both cards.",
    "strength": 4,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Philippa Eilhart",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Poor Fucking Infantry",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Tight Bond: Place next to a card with the same name to double the strength of both cards.",
    "strength": 1,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Sabrina Glavissing",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 4,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Siegried of Denesle",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Sile de Tansarville",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Sheldon Skaggs",
    "fraction": "Temeria",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 4,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Prince Stennis",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Spy: Place on your opponent's battlefield (counts towards opponent's total) and draw 2 cards from your deck.",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Thaler",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "Spy: Place on your opponent's battlefield (counts towards opponent's total) and draw 2 cards from your deck.",
    "strength": 1,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Esterad Thyssen",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Trebuchet",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "",
    "strength": 6,
    "quantity": 2,
    "hero": false
  }, {
    "name": "Vernon Roche",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Ves",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Siege Tower",
    "fraction": "Temeria",
    "type": "Siege Combat",
    "ability": "",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Yarpen Zigrin",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Redanian Foot Soldier",
    "fraction": "Temeria",
    "type": "Close Combat",
    "ability": "",
    "strength": 1,
    "quantity": 2,
    "hero": false
  }, {
    "name": "Arachas",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 4,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Grave Hag",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Cockatrice",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Arachas4",
    "fraction": "Monsters",
    "type": "Siege Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Fiend",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Draug",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Plague Maiden",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Endrega",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Forktail",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Frightener",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Ghoul",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 1,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Ice Giant",
    "fraction": "Monsters",
    "type": "Siege Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Gargoyle",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Griffin",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Calaeno Harpy",
    "fraction": "Monsters",
    "type": "Close/Ranged Combat",
    "ability": "Agile: Can be placed in either the Close Combat or the Ranged Combat row. Cannot be moved once placed.",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Harpy",
    "fraction": "Monsters",
    "type": "Close/Ranged Combat",
    "ability": "Agile: Can be placed in either the Close Combat or the Ranged Combat row. Cannot be moved once placed.",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Imlerith",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Kayran",
    "fraction": "Monsters",
    "type": "Close/Ranged Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.|Agile: Can be placed in either the Close Combat or the Ranged Combat row. Cannot be moved once placed.|Morale Boost: Adds +1 to all units in the row (excluding itself).",
    "strength": 8,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Leshen",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "Hero: Not affected by any Special Cards or abilities.",
    "strength": 10,
    "quantity": 1,
    "hero": true
  }, {
    "name": "Foglet",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Nekker",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 2,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Fire Elemental",
    "fraction": "Monsters",
    "type": "Siege Combat",
    "ability": "",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Botchling",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 4,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Vampire",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 4,
    "quantity": 5,
    "hero": false
  }, {
    "name": "Crone",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "Muster: Find any cards with the same name in your deck and play them instantly.",
    "strength": 6,
    "quantity": 3,
    "hero": false
  }, {
    "name": "Werewolf",
    "fraction": "Monsters",
    "type": "Close Combat",
    "ability": "",
    "strength": 5,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Wyvern",
    "fraction": "Monsters",
    "type": "Ranged Combat",
    "ability": "",
    "strength": 2,
    "quantity": 1,
    "hero": false
  }, {
    "name": "Earth Elemental",
    "fraction": "Monsters",
    "type": "Siege Combat",
    "ability": "",
    "strength": 6,
    "quantity": 1,
    "hero": false
  }
];
