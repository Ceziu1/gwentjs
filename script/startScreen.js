var deckPicked = false; //animacja wybranego decku
var currentDeck = ""; //zabezpieczenie wielokrotnego wyboru talii
var currentChampion = ""; //zabezpieczenie wielokrotnego wyboru championa

function startScreen() {
  var changeDir = true; //potrzebne zmianie kierunku układania talii
  var iterator = 1; //ustawianie odleglosci po roznych stronach osi
  var talia = ["Temeria", "Monsters"]; //dostepne telie
  var Temeria = ["Doubles the strength of all Siege units, unless a Commander's Horn is already in play on that row.", "Destroy your enemy's strongest Siege unit(s) if the combined strength of all his or her Siege units is 10 or more.", "Pick an Impenetrable Fog from your deck and play it instantly.", "Clear any weather effects (on both sides) currently in play."];
  var Monsters = ["Pick any weather card from your deck and play it instantly.", "Discard 2 cards and draw 1 card of your choice from your deck.", "Restore a card from your discard pile to your hand.", "Double the strength of all your Close Combat units (unless a Commander's Horn is also present on that row)."];
  //dostepni boaterowie
  var karta;
  var loader = new THREE.JSONLoader();

  function chooseDeck() {//sam początek po uruchomieniu
    canvas.clearCanvas(c.width, c.height);//buduje się i wybieramy talie ktorą chcemy grać
    canvas.returnCanvas("", 0, 0, "Choose your Deck.", c.width / 2, c.height / 2, 25);
    loader.load(
      'json/card.json',
      function(geometry) {
        for (var i = 0; i < talia.length; i++) {
          var texture = THREE.ImageUtils.loadTexture("deck/" + talia[i] + ".jpg");
          var material = new THREE.MeshBasicMaterial({
            map: texture,
            side: THREE.DoubleSide
          });
          var karta = new THREE.Mesh(geometry, material);
          changeDir = !changeDir;
          karta.rotation.set(90 * Math.PI / 180, 0, 0);
          if (changeDir) {
            karta.position.set(10 * iterator, 0, 0);
            iterator++;
          } else {
            karta.position.set(-(10 * iterator), 0, 0);
          }
          karta.scale.set(15, 1, 15);
          karta.name = talia[i];
          scene.add(karta);
        }
        iterator = 1;
        var geometry = new THREE.PlaneGeometry(5, 5, 32);
        var texture = THREE.ImageUtils.loadTexture("buttons/b_play.png");
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          transparent: true,
          side: THREE.DoubleSide,
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.name = "next";
        scene.add(plane);
      }
    );
  }

  this.pickDeck = function(deck, text) {//animacja wybranego decku do grania
    if (currentDeck === "" && deckPicked === false) {//duzo logiki i matematyki
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, text + ": " + deck.name, c.width / 2, 50, 24);
      var timer1 = setInterval(function() {
        if (deck.position.y > 5) {
          deckPicked = true;
          currentDeck = deck;
          clearInterval(timer1);
        }
        deck.position.y += 0.25;
      }, 20);
    } else if (currentDeck.name == deck.name && deckPicked) {
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, text + ": " + deck.name, c.width / 2, 50, 24);
    } else if (currentDeck.name != deck.name && deckPicked) {
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, text + ": " + deck.name, c.width / 2, 50, 24);
      var timer1 = setInterval(function() {
        currentDeck.position.y -= 0.25;
        deck.position.y += 0.25;
        if (currentDeck.position.y <= 0) {
          deckPicked = true;
          currentDeck = deck;
          clearInterval(timer1);
        }
      }, 20);
    }
    switch (deck.name) {
      case "Temeria":
        canvas.returnCanvas("", 0, 0, "Grants an extra card upon winning round", c.width / 2, c.height / 2, 15);
        break;
      case "Monsters":
        canvas.returnCanvas("", 0, 0, "Keeps random Unit Card out after each round", c.width / 2, c.height / 2, 15);
        break;
      default:
        break;
    }
  };

  this.clearScene = function(var1, array, turn) {//czyszczenie calej planszy
    var clear = setInterval(function() {
      for (var i = scene.children.length - 1; i >= 0; i--) {//jechanie po dzieckach
        scene.children[i].position.y += 1;
        if (scene.children[i].position.y > 33) {//jak znikna z monitora
          scene.remove(scene.children[i]);//to usuwam
          if (scene.children.length === 0) {
            clearInterval(clear);
            if (var1 == "loadHero")
              loadHero();//wyswietlenie na scenie dostepnych bohaterow
            else if (var1 == "summary")
              summary();//podsumowanie i czekanie na rpzeciwnika
            else if (var1 == "playField") {
              $(".waiting").remove();
              canvas.clearCanvas(c.width, c.height);
              socket.chooseTurn(turn);
              loadPlayField(array);
            }
          }
        }
      }
    }, 25);
  };

  function loadPlayField(array) {
    // ----------------------LIGHT-------------------------
    var pointLight = light.getLight("point", 0xFFCEAD);
    pointLight.position.set(13, 45, 1);
    scene.add(pointLight);
    pointLight = light.getLight("point", 0xFFCEAD);
    pointLight.position.set(-13, 45, 1);
    scene.add(pointLight);
    // ----------------------END LIGHT-------------------------
    loader.load(
      'json/board.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture("deck/textures/board.jpg");
        var material = new THREE.MeshPhongMaterial({
          color: 0xffa422,
          specular: 0x000000,
          shininess: 0,
          side: THREE.DoubleSide,
          map: texture
        });
        var board = new THREE.Mesh(geometry, material);
        board.name = "board";
        scene.add(board);
      }
    );
    loader.load(
      'json/Champion1.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture(currentChampion.material.map.sourceFile);
        var material = new THREE.MeshPhongMaterial({
          color: 0xffffff,
          specular: 0x000000,
          shininess: 0,
          side: THREE.DoubleSide,
          map: texture
        });
        var champion1 = new THREE.Mesh(geometry, material);
        champion1.name = currentChampion.name;
        scene.add(champion1);
      }
    );
    loader.load(
      'json/Deck1.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture(currentDeck.material.map.sourceFile);
        var material = new THREE.MeshPhongMaterial({
          color: 0xffffff,
          specular: 0x000000,
          shininess: 0,
          side: THREE.DoubleSide,
          map: texture
        });
        var deck1 = new THREE.Mesh(geometry, material);
        deck1.name = currentDeck.name;
        scene.add(deck1);
      }
    );
    loader.load(
      'json/Deck2.json',
      function(geometry) {
        var name, texture;
        if (array[0].id == id) {
          texture = THREE.ImageUtils.loadTexture("/deck/" + array[1].deck + ".jpg");
          name = array[1].deck;
        } else {
          texture = THREE.ImageUtils.loadTexture("/deck/" + array[0].deck + ".jpg");
          name = array[0].deck;
        }
        var material = new THREE.MeshPhongMaterial({
          color: 0xffffff,
          specular: 0x000000,
          shininess: 0,
          side: THREE.DoubleSide,
          map: texture
        });
        var deck1 = new THREE.Mesh(geometry, material);
        enemyDeck = name;
        deck1.name = name;
        scene.add(deck1);
        socket.drawDeckCards();
      }
    );
    loader.load(
      'json/Champion2.json',
      function(geometry) {
        var name, texture;
        if (array[0].id == id) {
          if (array[1].deck == "Temeria") {
            texture = THREE.ImageUtils.loadTexture("/deck/" + array[1].deck + "/champion/" + (parseInt(Temeria.indexOf(array[1].champion)) + 1) + ".png");
            name = array[1].champion;
          } else {
            texture = THREE.ImageUtils.loadTexture("/deck/" + array[1].deck + "/champion/" + (parseInt(Monsters.indexOf(array[1].champion)) + 1) + ".png");
            name = array[1].champion;
          }
        } else {
          if (array[0].deck == "Temeria") {
            texture = THREE.ImageUtils.loadTexture("/deck/" + array[0].deck + "/champion/" + (parseInt(Temeria.indexOf(array[0].champion)) + 1) + ".png");
            name = array[0].champion;
          } else {
            texture = THREE.ImageUtils.loadTexture("/deck/" + array[0].deck + "/champion/" + (parseInt(Monsters.indexOf(array[0].champion)) + 1) + ".png");
            name = array[0].champion;
          }
        }
        var material = new THREE.MeshPhongMaterial({
          color: 0xffffff,
          specular: 0x000000,
          shininess: 0,
          side: THREE.DoubleSide,
          map: texture
        });
        var champion2 = new THREE.Mesh(geometry, material);
        champion2.name = name;
        scene.add(champion2);
      }
    );
    loader.load('json/candle.json', function(geometry, materials) {
      var material = new THREE.MeshFaceMaterial(materials);
      model = new THREE.Mesh(geometry, material);
      scene.add(model);
    });
    camera.position.y = 15;
    camera.position.z = 3;
    camera.lookAt(scene.position);
    camera.position.z = 6;
    camera.rotation.z = 0;
  }

  function loadHero() {//to samo co chooseDeck tylko ze na bohaterach
    deckPicked = false;
    canvas.clearCanvas(c.width, c.height);
    canvas.returnCanvas("", 0, 0, "Choose your champion.", c.width / 2, c.height / 2, 25);
    loader.load(
      'json/card.json',
      function(geometry) {
        for (var i = 1; i <= Temeria.length; i++) {
          var texture = THREE.ImageUtils.loadTexture("deck/" + currentDeck.name + "/champion/" + i + ".png");
          var material = new THREE.MeshBasicMaterial({
            map: texture,
            side: THREE.DoubleSide
          });
          var karta = new THREE.Mesh(geometry, material);
          changeDir = !changeDir;
          karta.rotation.set(90 * Math.PI / 180, 0, 0);
          if (changeDir) {
            karta.position.set(10 * iterator, 0, 0);
            iterator++;
          } else {
            karta.position.set(-(10 * iterator), 0, 0);
          }
          karta.scale.set(15, 1, 15);
          if (currentDeck.name == "Temeria") {
            karta.name = Temeria[i - 1];
          } else if (currentDeck.name == "Monsters") {
            karta.name = Monsters[i - 1];
          }
          scene.add(karta);
        }
        var geometry = new THREE.PlaneGeometry(5, 5, 32);
        var texture = THREE.ImageUtils.loadTexture("buttons/b_play.png");
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          transparent: true,
          side: THREE.DoubleSide,
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.name = "next";
        scene.add(plane);
      }
    );
  }

  this.pickHero = function(championName) {//to samo co pickDeck tylko na bohaterach
    if (currentChampion === "" && deckPicked === false) {
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, championName.name, c.width / 2, c.height / 2, 25);
      var timer1 = setInterval(function() {
        if (championName.position.y > 5) {
          deckPicked = true;
          currentChampion = championName;
          clearInterval(timer1);
        }
        championName.position.y += 0.25;
      }, 20);
    } else if (currentChampion.name == championName.name && deckPicked) {
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, championName.name, c.width / 2, c.height / 2, 25);
    } else if (currentChampion.name != championName.name && deckPicked) {
      canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("", 0, 0, championName.name, c.width / 2, c.height / 2, 25);
      var timer1 = setInterval(function() {
        currentChampion.position.y -= 0.25;
        championName.position.y += 0.25;
        if (currentChampion.position.y <= 0) {
          deckPicked = true;
          currentChampion = championName;
          clearInterval(timer1);
        }
      }, 20);
    }
  };

  function summary() {//podsumowanie po wybraniu bohatera i desku
    deckPicked = false;
    canvas.clearCanvas(c.width, c.height);
    canvas.returnCanvas("", 0, 0, "Waiting for enemy.", c.width / 2, 40, 15);
    canvas.returnCanvas("", 0, 0, "Your deck: " + currentDeck.name, c.width / 2, 80, 20);
    canvas.returnCanvas("", 0, 0, "Champion ability: " + currentChampion.name, c.width / 2, 120, 20);
    loader.load(
      'json/card.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture(currentChampion.material.map.image.src);
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        var karta = new THREE.Mesh(geometry, material);
        karta.rotation.set(90 * Math.PI / 180, 0, 0);
        karta.position.set(-10, 0, 0);
        karta.scale.set(15, 1, 15);
        scene.add(karta);

        var texture = THREE.ImageUtils.loadTexture(currentDeck.material.map.image.src);
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        var karta = new THREE.Mesh(geometry, material);
        karta.rotation.set(90 * Math.PI / 180, 0, 0);
        karta.position.set(10, 0, 0);
        karta.scale.set(15, 1, 15);
        scene.add(karta);
      }
    );
    var waiting = document.createElement("img");
    waiting.src = "buttons/waiting.png";
    waiting.className = "waiting";
    document.body.appendChild(waiting);
    socket.waitingForPlayer();
  }

  chooseDeck();
}
