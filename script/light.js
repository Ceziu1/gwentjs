function Light() {//takie proste ze chyba nie musze tlumaczyc
  this.getLight = function(type, color) {
    switch (type) {
      case "point":
        return new THREE.PointLight(color, 1, 100);
      case "spot":
        return new THREE.SpotLight(color, 10);
      default:
        break;
    }
  };
}
