var scene;

function Main() { //to chyba znasz
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(
    45, // kąt patrzenia kamery (FOV - field of view)
    window.innerWidth / window.innerHeight, // proporcje widoku
    0.1, // min renderowana odległość
    10000 // max renderowana odległość
  );
  renderer = new THREE.WebGLRenderer({
    alpha: true
  });
  renderer.setClearColor(0x000000, 0.7);
  renderer.setSize(innerWidth, innerHeight);
  document.getElementById("content").appendChild(renderer.domElement);
  camera.position.x = 0;
  camera.position.y = 0;
  camera.position.z = 50;
  camera.lookAt(scene.position);
  animateScene();

  function animateScene() {
    if (playerSurrender && enemySurrender) {
      socket.endPlay();
    }
    camera.updateProjectionMatrix();
    requestAnimationFrame(animateScene);
    renderer.render(scene, camera);
  }
}
