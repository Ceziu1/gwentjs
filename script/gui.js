var boardDeck = []; //wszystkie karty w talii
var handDeck = []; //karty w rece
var cardToChange = 0;
var currnetModify = "";
var cardToPlay; //odpowiada za wystawienie karty na stoł
var loader = new THREE.JSONLoader();
var handContent = new THREE.Object3D(); //tutaj umieszcza sie karty na stole
var weather = new THREE.Object3D();
var playerCloseCombat = new THREE.Object3D();
var playerRangedCombat = new THREE.Object3D();
var playerSiegeCombat = new THREE.Object3D();
var enemyCloseCombat = new THREE.Object3D();
var enemyRangedCombat = new THREE.Object3D();
var enemySiegeCombat = new THREE.Object3D();
jaskier = enemyJaskier = false;
moraleBooster = enemyMoraleBooster = 0;

function GUI() {
  var changeDir = false,
    iterator = 1;

  this.countPoints = function() { //obliczanie punktow
    playerScore = enemyScore = 0;
    playerCloseCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0) {
        playerScore += JSON.parse(item.name).strength;
      } else if (jaskier && scene.getChildByName("ice") === undefined) {
        playerScore += JSON.parse(item.name).strength * 2;
      } else if (jaskier && scene.getChildByName("ice") !== undefined) {
        playerScore += 2;
      } else if (scene.getChildByName("ice") === undefined) {
        playerScore += JSON.parse(item.name).strength;
      } else {
        playerScore += 1;
      }
    });
    playerRangedCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0 || scene.getChildByName("fog") === undefined) {
        playerScore += JSON.parse(item.name).strength;
      } else {
        playerScore += 1;
      }
    });
    if (moraleBooster) {
      playerScore += playerSiegeCombat.children.length * moraleBooster;
    }
    playerSiegeCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0 || scene.getChildByName("rain") === undefined) {
        playerScore += JSON.parse(item.name).strength;
      } else {
        playerScore += 1;
      }
    });
    enemyCloseCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0) {
        enemyScore += JSON.parse(item.name).strength;
      } else if (enemyJaskier && scene.getChildByName("ice") === undefined) {
        enemyScore += JSON.parse(item.name).strength * 2;
      } else if (enemyJaskier && scene.getChildByName("ice") !== undefined) {
        enemyScore += 2;
      } else if (scene.getChildByName("ice") === undefined) {
        enemyScore += JSON.parse(item.name).strength;
      } else {
        enemyScore += 1;
      }
    });
    enemyRangedCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0 || scene.getChildByName("fog") === undefined) {
        enemyScore += JSON.parse(item.name).strength;
      } else {
        enemyScore += 1;
      }
    });
    enemySiegeCombat.children.forEach(function(item) {
      if (JSON.parse(item.name).ability.indexOf("Hero") >= 0 || scene.getChildByName("rain") === undefined) {
        enemyScore += JSON.parse(item.name).strength;
      } else {
        enemyScore += 1;
      }
    });
    if (enemyMoraleBooster) {
      enemyScore += enemySiegeCombat.children.length * enemyMoraleBooster;
    }
  };

  this.draw10Cards = function(serverDeck) { //budowanie modeli kart trzymanych w ręce
    for (var i = 0; i < serverDeck.length; i++) {
      if (serverDeck[i].quantity > 1) {
        for (var j = 0; j < serverDeck[i].quantity; j++) {
          var tempObject = (JSON.parse(JSON.stringify(serverDeck[i])));
          tempObject.name += (j + 1);
          boardDeck.push(tempObject);
        }
      } else {
        boardDeck.push(serverDeck[i]);
      }
    } //koniec fora - odpowiada za spushowanie do tablicy wszystkich kart
    for (var k = 0; k < playerHandLength; k++) {
      var ri = Math.floor(Math.random() * boardDeck.length); // Random Index position in the array
      handDeck.push(boardDeck.splice(ri, 1)[0]); // Splice out a random element using the ri var
    } //koniec fora - losowanie 10 kart z naszej talii
    loader.load(
      'json/card.json',
      function(geometry) {
        for (var i = 0; i < handDeck.length; i++) {
          var texture = THREE.ImageUtils.loadTexture("deck/" + handDeck[i].fraction + "/" + handDeck[i].name + ".png");
          var material = new THREE.MeshBasicMaterial({
            map: texture,
            side: THREE.DoubleSide
          });
          var karta = new THREE.Mesh(geometry, material);
          karta.scale.set(2.5, 1, 2.5);
          karta.name = JSON.stringify(handDeck[i]);
          handContent.add(karta);
          changeDir = !changeDir;
          if (i === 0) {
            karta.position.set(0, 0, 0);
          } else if (changeDir) {
            karta.position.set(1.45 * iterator, 0, 0);
            iterator++;
          } else {
            karta.position.set(-(1.45 * iterator), 0, 0);
          }
        }
      }
    ); //koniec JSON.loader - budowanie, texturowanie i ustawianie kart z ręki
    handContent.position.z = 6;
    scene.add(handContent);
    weather.position.z = positionArray[7].z;
    weather.position.x = positionArray[7].x;
    scene.add(weather);
    playerCloseCombat.position.z = positionArray[0].z;
    playerCloseCombat.position.x = positionArray[0].x;
    scene.add(playerCloseCombat);
    playerRangedCombat.position.z = positionArray[1].z - 0.1;
    playerRangedCombat.position.x = positionArray[1].x;
    scene.add(playerRangedCombat);
    playerSiegeCombat.position.z = positionArray[2].z - 0.1;
    playerSiegeCombat.position.x = positionArray[2].x;
    scene.add(playerSiegeCombat);
    enemyCloseCombat.position.z = positionArray[3].z;
    enemyCloseCombat.position.x = positionArray[3].x;
    scene.add(enemyCloseCombat);
    enemyRangedCombat.position.z = -positionArray[1].z + 0.1;
    enemyRangedCombat.position.x = positionArray[1].x;
    scene.add(enemyRangedCombat);
    enemySiegeCombat.position.z = -positionArray[2].z + 0.2;
    enemySiegeCombat.position.x = positionArray[2].x;
    scene.add(enemySiegeCombat);
    //ustalanie pozycji Object3D
    currnetModify = "changeCard";
    canvas.returnCanvas("", 0, 0, "Choose " + cardToChange + "/3 cards from hand to change or press ESC to play.", 700, 100, 25);
  };

  this.draw1Card = function() { //dobieranie karty z talii
    loader.load(
      'json/card.json',
      function(geometry) {
        var ri = Math.floor(Math.random() * (boardDeck.length - 1)); // Random Index position in the array
        try {
          handDeck.push(JSON.parse(boardDeck.splice(ri, 1)[0])); // Splice out a random element using the ri var
        } catch (e) {
          handDeck.push(boardDeck.splice(ri, 1)[0]); // Splice out a random element using the ri var
        }
        var texture = THREE.ImageUtils.loadTexture("deck/" + handDeck[handDeck.length - 1].fraction + "/" + handDeck[handDeck.length - 1].name + ".png");
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        var karta = new THREE.Mesh(geometry, material);
        karta.scale.set(2.5, 1, 2.5);
        karta.name = JSON.stringify(handDeck[handDeck.length - 1]);
        handContent.add(karta);
        gui.recalculatePositions("hand"); //przeliczanie pozycji aby karty nie nachodziły do siebie (pisaliśmy razem)
        socket.enemyHandCount(handDeck.length);
      });
  };

  this.recalculatePositions = function(arg) { //przeliczanie pozycji kart aby nie nachodziły na siebie
    var changeDir = false, //argument przyjmowany odpowiada za zidentyfikowanie ktre pole chcemy przeliczyć
      iterator = 1;
    switch (arg) {
      case "hand":
        for (var i = 0; i < handContent.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            handContent.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            handContent.children[i].position.set(1.45 * iterator, 0, 0);
            iterator++;
          } else {
            handContent.children[i].position.set(-(1.45 * iterator), 0, 0);
          }
        }
        break;
      case "playerCloseCombat":
        for (var i = 0; i < playerCloseCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            playerCloseCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            playerCloseCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            playerCloseCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "enemyCloseCombat":
        for (var i = 0; i < enemyCloseCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            enemyCloseCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            enemyCloseCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            enemyCloseCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "playerRangedCombat":
        for (var i = 0; i < playerRangedCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            playerRangedCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            playerRangedCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            playerRangedCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "enemyRangedCombat":
        for (var i = 0; i < enemyRangedCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            enemyRangedCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            enemyRangedCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            enemyRangedCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "playerSiegeCombat":
        for (var i = 0; i < playerSiegeCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            playerSiegeCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            playerSiegeCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            playerSiegeCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "enemySiegeCombat":
        for (var i = 0; i < enemySiegeCombat.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            enemySiegeCombat.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            enemySiegeCombat.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            enemySiegeCombat.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      case "weather":
        for (var i = 0; i < weather.children.length; i++) {
          changeDir = !changeDir;
          if (i === 0) {
            weather.children[i].position.set(0, 0, 0);
          } else if (changeDir) {
            weather.children[i].position.set(0.6 * iterator, 0, 0);
            iterator++;
          } else {
            weather.children[i].position.set(-(0.6 * iterator), 0, 0);
          }
        }
        break;
      default:
        break;
    }
  };

  this.addLight = function(type) { //zarządzanie światłami aby ułatwić poruszanie się po planszy
    var combatLight = light.getLight("spot", 0xFFFFFF);
    switch (type) {
      case "Close Combat":
        combatLight.position.set(positionArray[0].x, 1, positionArray[0].z);
        break;
      case "Ranged Combat":
        combatLight.position.set(positionArray[1].x, 1, positionArray[1].z);
        break;
      case "Siege Combat":
        combatLight.position.set(positionArray[2].x, 1, positionArray[2].z);
        break;
      case "Torrential Rain1": //TAK SIĘ MUSZĄ NAZYWAĆ ARGUMENTY
      case "Torrential Rain2": //WYNIAK TO ZE STRUKTURY PLIKOW
        // case "Torrential Rain3": //KAZDY MESH JEST NAZWANY TAK JAK JEST W BAZIE
      case "Impenetrable Fog1": //WYSTARCZY SPARSOWAĆ JSONem I DAC ODPOWIEDNIE ODWOLANIE
      case "Impenetrable Fog2":
        // case "Impenetrable Fog3":
      case "Bitting Frost1":
      case "Bitting Frost2":
        // case "Bitting Frost3":
      case "Clear Weather1":
      case "Clear Weather2":
        // case "Clear Weather3":
        combatLight.position.set(positionArray[7].x, 1, positionArray[7].z);
        break;
      case "Close/Ranged Combat":
        combatLight.position.set(positionArray[0].x, 1, positionArray[0].z);
        break;
      case "Commander's Horn1":
      case "Commander's Horn2":
        // case "Commander's Horn3":
        combatLight.position.set(positionArray[4].x, 1, positionArray[4].z);
        break;
      default:
        return;
    }
    combatLight.target.position.set(combatLight.position.x, 0, combatLight.position.z);
    scene.add(combatLight.target);
    combatLight.name = "combatLight";
    scene.add(combatLight);
  };

  this.removeLight = function() { //usuwanie światła ze sceny
    scene.remove(scene.getObjectByName("combatLight"));
  };

  this.addCard = function(cardToPlay, lightPosition) { //dodanie karty z ręki na pole bitwy
    loader.load(
      'json/card.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture("deck/" + cardToPlay.fraction + "/" + cardToPlay.name + ".png");
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        var karta = new THREE.Mesh(geometry, material);
        karta.scale.set(1, 1, 1);
        karta.name = JSON.stringify(cardToPlay);
        if (cardToPlay.ability.indexOf("Spy") !== -1) {
          gui.draw1Card();
          gui.draw1Card();
        }
        if (cardToPlay.ability.indexOf("Muster") !== -1) {
          for (var i = 0; i < handDeck.length; i++) {
            if (handDeck[i].name.indexOf(cardToPlay.name.replace(/\d+/g, '')) >= 0) { // && cardToPlay.name !== card[0].name
              var card = handDeck.splice(i, 1);
              if (cardToPlay.name !== card[0].name) {
                handContent.remove(scene.getObjectByName(JSON.stringify(card[0])));
                socket.musted(card[0]);
                gui.addCard(card[0]);
              } else {
                handDeck.splice(i, 0, card[0]);
              }
            }
          }
          for (var i = 0; i < boardDeck.length; i++) {
            if (boardDeck[i].name.indexOf(cardToPlay.name.replace(/\d+/g, '')) >= 0) { // && cardToPlay.name !== card[0].name
              console.warn(boardDeck[i].name);
              var card = boardDeck.splice(i, 1);
              console.log(card[0]);
              socket.musted(card[0]);
              gui.addCard(card[0]);
            }
          }
        }
        gui.recalculatePositions("hand");
        switch (cardToPlay.type) {
          case "Close Combat":
            playerCloseCombat.add(karta);
            gui.recalculatePositions("playerCloseCombat");
            break;
          case "Close/Ranged Combat":
            if (lightPosition === 0.8) {
              playerCloseCombat.add(karta);
              gui.recalculatePositions("playerCloseCombat");
            } else if (lightPosition === 2.2) {
              playerRangedCombat.add(karta);
              gui.recalculatePositions("playerRangedCombat");
            }
            break;
          case "Ranged Combat":
            playerRangedCombat.add(karta);
            gui.recalculatePositions("playerRangedCombat");
            break;
          case "Siege Combat":
            playerSiegeCombat.add(karta);
            gui.recalculatePositions("playerSiegeCombat");
            break;
          case "Weather":
            weather.add(karta);
            addWeatherEffect(karta.name);
            gui.recalculatePositions("weather");
            break;
          default:
            break;
        }
        socket.enemyHandCount(handDeck.length);
      });
  };

  this.updatePlayField = function(data, position) { //uzywana przez drugiego klienta ktory aktualnie nie grał
    loader.load( //dostaje dane od serwera aby wiedzial jak ma zbudować nową karte
      'json/card.json',
      function(geometry) {
        var texture = THREE.ImageUtils.loadTexture("deck/" + data.fraction + "/" + data.name + ".png");
        var material = new THREE.MeshBasicMaterial({
          map: texture,
          side: THREE.DoubleSide
        });
        var karta = new THREE.Mesh(geometry, material);
        karta.scale.set(1, 1, 1);
        karta.name = JSON.stringify(data);
        switch (data.type) {
          case "Close Combat":
            enemyCloseCombat.add(karta);
            gui.recalculatePositions("enemyCloseCombat");
            break;
          case "Close/Ranged Combat":
            if (position === 0.8) {
              enemyCloseCombat.add(karta);
              gui.recalculatePositions("enemyCloseCombat");
            } else if (position === 2.2) {
              enemyRangedCombat.add(karta);
              gui.recalculatePositions("enemyRangedCombat");
            }
            break;
          case "Ranged Combat":
            enemyRangedCombat.add(karta);
            gui.recalculatePositions("enemyRangedCombat");
            break;
          case "Siege Combat":
            enemySiegeCombat.add(karta);
            gui.recalculatePositions("enemySiegeCombat");
            break;
          case "Weather":
            weather.add(karta);
            addWeatherEffect(karta.name);
            gui.recalculatePositions("weather");
            break;
          default:
            break;
        }
      });
  };

  function addWeatherEffect(weatherCard) {
    var wcn = JSON.parse(weatherCard).name;
    switch (wcn) {
      case 'Impenetrable Fog1':
      case 'Impenetrable Fog2':
        if (scene.getChildByName("fog") === undefined) {
          loader.load(
            'json/fog.json',
            function(geometry) {
              var texture = THREE.ImageUtils.loadTexture("json/fog.png");
              var material = new THREE.MeshBasicMaterial({
                map: texture,
                transparent: true
              });
              var object = new THREE.Mesh(geometry, material);
              object.name = "fog";
              object.scale.set(1, 1, 1);
              scene.add(object);
              var fogMove = true;

              function updateTexture() {
                requestAnimationFrame(updateTexture);
                if (object.material.map.offset.x > 0.3)
                  fogMove = false;
                if (object.material.map.offset.x < 0)
                  fogMove = true;
                if (fogMove)
                  object.material.map.offset.x += 0.0003;
                else
                  object.material.map.offset.x -= 0.0003;
              }
              updateTexture();
            }
          );
        } else {
          weather.remove(weather.getChildByName(weatherCard));
        }
        break;
      case 'Torrential Rain1':
      case 'Torrential Rain2':
        if (scene.getChildByName("rain") === undefined) {
          loader.load(
            'json/rain.json',
            function(geometry) {
              var texture = THREE.ImageUtils.loadTexture("json/rain.png");
              var material = new THREE.MeshBasicMaterial({
                map: texture,
                transparent: true,
              });
              var object = new THREE.Mesh(geometry, material);
              object.name = "rain";
              object.scale.set(1, 1, 1);
              scene.add(object);

              function updateTexture() {
                requestAnimationFrame(updateTexture);
                object.material.map.offset.y += 0.005;
                if (object.material.map.offset.y > 0.5)
                  object.material.map.offset.y = 0;
              }
              updateTexture();
            }
          );
        } else {
          weather.remove(weather.getChildByName(weatherCard));
        }
        break;
      case 'Bitting Frost1':
      case 'Bitting Frost2':
        if (scene.getChildByName("ice") === undefined) {
          loader.load(
            'json/ice.json',
            function(geometry) {
              var texture = THREE.ImageUtils.loadTexture("json/ice.png");
              var material = new THREE.MeshBasicMaterial({
                map: texture,
                transparent: true
              });
              var object = new THREE.Mesh(geometry, material);
              object.name = "ice";
              object.scale.set(1, 1, 1);
              scene.add(object);
            }
          );
        } else {
          weather.remove(weather.getChildByName(weatherCard));
        }
        break;
      case 'Clear Weather1':
      case 'Clear Weather2':
        setTimeout(function() {
          weather.children.length = 0;
          scene.remove(scene.getChildByName("ice"));
          scene.remove(scene.getChildByName("fog"));
          scene.remove(scene.getChildByName("rain"));
          gui.countPoints();
          canvas.clearCanvas(c.width, c.height);
          canvas.updateStatus();
        }, 1000);
        break;
    }
  }
}
