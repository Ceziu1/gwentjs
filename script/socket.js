var id, playerTurn;

function Socket() {
  client = io();

  client.on("onconnect", function(data) {
    id = data.clientName; //pozniej sie okaze nieprzydatne
  });

  this.waitingForPlayer = function() { //wyslanie do serwera danych z wybranym bohaterem i talią
    client.emit("ready", {
      deck: currentDeck.name,
      champion: currentChampion.name
    });
  };

  this.drawDeckCards = function() { //ządanie dostepu do bazy z wybraną talią
    client.emit("drawCards", {
      deck: currentDeck.name
    });
  };

  this.jaskier = function() { //emisja, że użyto jaskra (do podwojenia wartości punktów)
    client.emit("jaskier", {});
  };
  this.morale = function() { //emisja, że użyto jaskra (do podwojenia wartości punktów)
    client.emit("morale", {});
  };

  this.chooseTurn = function(turn) { //to robiliśmy razem
    var coinImage = new Image(); //na podstawie zmiennej ustalamy
    playerTurn = turn; //kto pierwszy zaczyna

    if (turn) {
      coinImage.src = "buttons/you.png";
      canvas.returnCanvas(coinImage.src, c.width / 2 - 64, 0, '', 0, 0, 0);
      canvas.returnCanvas('', 0, 0, 'You will go first', c.width / 2, 150, 20);
    } else {
      coinImage.src = "buttons/enemy.png";
      canvas.returnCanvas(coinImage.src, c.width / 2 - 64, 0, '', 0, 0, 0);
      canvas.returnCanvas('', 0, 0, 'Enemy will go first', c.width / 2, 150, 20);
    }
  };

  this.endTurn = function(card, position) { //wyslanie na serwer wybranej karty na koniec tury
    socket.enemyHandCount(handDeck.length);
    client.emit("endTurn", {
      card: card,
      position: position,
    });
  };

  this.enemyHandCount = function(hand) {
    client.emit("hand", {
      hand: hand
    });
  };

  this.musted = function(card) {
    client.emit("musted", {
      card: card
    });
  };

  this.surrender = function() {
    playerTurn = false;
    client.emit("surrender", {});
    canvas.clearCanvas(c.width, c.height);
    canvas.updateStatus();
  };

  this.endGame = function() {
    document.removeEventListener("mousedown", rays.rays, false);
    // setTimeout(function() {
    canvas.clearCanvas(c.width, c.height);
    if (playerLife === 0 && enemyLife === 0) {
      canvas.returnCanvas("buttons/status/draw.png", (c.width / 2 - 175), 0, "", 0, 0, 0);
      return;
    }
    if (enemyLife === 0) {
      canvas.returnCanvas("buttons/status/victory.png", (c.width / 2 - 175), 0, "", 0, 0, 0);
    } else if (playerLife === 0) {
      canvas.returnCanvas("buttons/status/loose.png", (c.width / 2 - 170), 0, "", 0, 0, 0);
    }
    canvas.returnCanvas("", 0, 0, "Your score: " + playerScore, (c.width / 2 - 250), 125, 25);
    canvas.returnCanvas("", 0, 0, "Enemy score: " + enemyScore, (c.width / 2 + 250), 125, 25);
    // }, 1000);
  };

  this.endPlay = function() {
    enemySurrender = playerSurrender = false;
    if (playerScore === enemyScore) {
      playerLife -= 1;
      enemyLife -= 1;
    } else if (playerScore > enemyScore) {
      enemyLife -= 1;
      canvas.returnCanvas("buttons/status/winRound.png", (c.width / 2 - 145), 0, "", 0, 0, 0);
	    canvas.returnCanvas("", 0, 0, "You win this round", (c.width / 2 + 190), 125, 25);
    } else {
      playerLife -= 1;
	  canvas.clearCanvas(c.width, c.height);
      canvas.returnCanvas("buttons/status/looseRound.png", (c.width / 2 - 85), 0, "", 0, 0, 0);
	    canvas.returnCanvas("", 0, 0, "You loose this round", (c.width / 2 + 190), 125, 25);
    }
		setTimeout(function(){
		  if (playerLife === 0 || enemyLife === 0) {
		    socket.endGame();
		  } else{
				jaskier= enemyJaskier= false;
				moraleBooster = enemyMoraleBooster = 0;
				playerScore = enemyScore = 0;
				weather.children.length = 0;
				playerCloseCombat.children.length = 0;
				playerRangedCombat.children.length = 0;
				playerSiegeCombat.children.length = 0;
				enemyCloseCombat.children.length = 0;
				enemyRangedCombat.children.length = 0;
				enemySiegeCombat.children.length = 0;
		    scene.remove(scene.getChildByName("ice"));
		    scene.remove(scene.getChildByName("fog"));
		    scene.remove(scene.getChildByName("rain"));
			
			  canvas.clearCanvas(c.width, c.height);
				canvas.updateStatus();
				canvas.showTurn();
			}
		}, 1500)
  };

  client.on("surrender", function() { //poleceni od serwera ze nastepuje zmiana tury
    enemySurrender = true;
    playerTurn = true;
    canvas.clearCanvas(c.width, c.height);
    canvas.updateStatus();
    canvas.showTurn();
  });

  client.on("hand", function(data) { //poleceni od serwera ze nastepuje zmiana tury
    enemyHandLength = data.hand;
  });

  client.on("musted", function(data) { //poleceni od serwera ze nastepuje zmiana tury
    gui.updatePlayField(data.card);
  });

  client.on("changeTurn", function() { //poleceni od serwera ze nastepuje zmiana tury
    if (handDeck.length === 0) {
      playerTurn = false;
      playerSurrender = true;
      client.emit("surrender", {});
      canvas.clearCanvas(c.width, c.height);
      canvas.updateStatus();
      return;
    } else if (playerSurrender || enemySurrender) {
      return;
    }
    playerTurn = !playerTurn;
  });

  client.on("jaskier", function() { //poleceni od serwera ze nastepuje zmiana tury
    enemyJaskier = !enemyJaskier;
  });

  client.on("morale", function() { //poleceni od serwera ze nastepuje zmiana tury
    enemyMoraleBooster++;
  });

  client.on("updatePlayField", function(data) { //serwer dostarczyl dane o tym czym zagrał przeciwnik
    gui.updatePlayField(JSON.parse(data.card.card), JSON.parse(data.card.position));
    setTimeout(function() {
      gui.countPoints(); //zliczanie punktow przeciwnika
      canvas.clearCanvas(c.width, c.height);
      canvas.updateStatus();
      canvas.showTurn();
    }, 500);
  });

  client.on("10Cards", function(data) { //dostalismy 10 kart od serwera
    playerHandLength = enemyHandLength = 10; //ilosc kart w ręce
    playerLife = enemyLife = 2; //zycia
    playerScore = enemyScore = 0; //punkty
    playerSurrender = enemySurrender = false; //czy przeciwnik się poddał
    setTimeout(function() { //timeout zeby mozna bylo zobaczyc kto zaczyna
      canvas.clearCanvas(c.width, c.height);
      gui.draw10Cards(JSON.parse(data.deck));
      canvas.updateStatus();
    }, 5000);
  });

  client.on("moveToPlayfield", function(data) {
    if (data.player) {
      intro.clearScene("playField", JSON.parse(data.enemy), data.turn); //mozna zacząć budować playfield
    }
  });
}
