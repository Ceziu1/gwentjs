var positionArray = [{
  x: 1.7, //close combat
  z: 0.8
}, {
  x: 1.7, //ranged combat
  z: 2.2
}, {
  x: 1.7, //siege combat
  z: 3.6
}, {
  x: 1.7, //spy, enemy close combat
  z: -0.8
}, {
  x: -3.1, //close combat morale
  z: 0.8
}, {
  x: -3.1, //ranged combat morale
  z: 2.2
}, {
  x: -3.1, //siege combat morale
  z: 3.6
}, {
  x: -6.75, //weather card
  z: 0
}];

function Rays() {

  var raycaster = new THREE.Raycaster(); // obiekt symulujący "rzucanie" promieni
  var mouseVector = new THREE.Vector2(); // wektor (x,y) wykorzystany bedzie do określenie pozycji myszy na ekranie

  this.rays = function(event) {
    mouseVector.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouseVector.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouseVector, camera);
    var intersects = raycaster.intersectObjects(scene.children, true);
    if (intersects.length > 0) {
      if (intro.pickDeck !== undefined) {
        if (deckPicked === false && intersects[0].object.name == "next") {
          return;
        } else if (deckPicked === true && intersects[0].object.name == "next") {
          intro.clearScene("loadHero");
          intro.pickDeck = undefined;
          return;
        }
        intro.pickDeck(intersects[0].object, "Deck");
      } else if (intro.pickHero !== undefined) {
        if (deckPicked === false && intersects[0].object.name == "next") {
          return;
        } else if (deckPicked === true && intersects[0].object.name == "next") {
          intro.clearScene("summary");
          intro.pickHero = undefined;
          return;
        }
        intro.pickHero(intersects[0].object);
      } else {
        if (currnetModify === "changeCard") {
          if (intersects[0].object.parent == handContent) {
            console.log(intersects[0].object.name);
            try {
              for (var i = 0; i < handDeck.length; i++) {
                if (handDeck[i].name.indexOf(JSON.parse(intersects[0].object.name).name) >= 0) {
                  console.log(handDeck[i]);
                  handDeck.splice(i, 1);
                  break;
                }
              }
              boardDeck.push((JSON.parse(JSON.stringify(intersects[0].object.name))));
              var selectedObject = scene.getObjectByName(intersects[0].object.name);
              handContent.remove(selectedObject);
              gui.draw1Card();
              cardToChange++;
              canvas.clearCanvas(c.width, c.height);
              canvas.updateStatus();
              if (cardToChange < 3) {
                canvas.returnCanvas("", 0, 0, "Choose " + cardToChange + "/3 cards from hand to change or press ESC to play.", 700, 100, 25);
              } else {
                canvas.showTurn();
                currnetModify = "beginPlay";
              }
            } catch (e) {
              console.log(e);
              console.log(intersects[0].object.name);
            }
          }
        } else if (currnetModify === "beginPlay") {
          if (intersects[0].object.parent != handContent) {
            cardToPlay = "";
            var texture;
            try {
              console.log(JSON.parse(intersects[0].object.name));
              texture = THREE.ImageUtils.loadTexture("deck/" + JSON.parse(intersects[0].object.name).fraction + "/" + JSON.parse(intersects[0].object.name).name + ".png");
              canvas.clearCanvas(c.width, c.height);
              canvas.updateStatus();
              var card = JSON.parse(intersects[0].object.name);
              canvas.returnCanvas("", 0, 0, "Name: " + card.name, c.width / 2, 25, 20);
              if (cardToPlay.type === "Weather" || card.type === "Horn") {
                canvas.returnCanvas("", 0, 0, "Ability: " + card.ability, c.width / 2, 50, 20);
                canvas.returnCanvas("", 0, 0, "Press ESC to close view.", c.width / 2, 75, 20);
              } else {
                canvas.returnCanvas("", 0, 0, "Type: " + card.type, c.width / 2, 50, 20);
                for (var i = 0; i < card.ability.split("|").length; i++) {
                  canvas.returnCanvas("", 0, 0, "Ability: " + card.ability.split("|")[i], c.width / 2, 75 + 25 * i, 15);
                }
                canvas.returnCanvas("", 0, 0, "Strength: " + card.strength, c.width / 2, 150, 20);
                canvas.returnCanvas("", 0, 0, "Press ESC to close view.", c.width / 2, 175, 20);
              }
            } catch (e) {
              console.log(e);
              console.log(intersects[0].object.name);
              switch (intersects[0].object.name) {
                case "fog":
                case "rain":
                case "ice":
                  texture = THREE.ImageUtils.loadTexture("deck/" + JSON.parse(intersects[1].object.name).fraction + "/" + JSON.parse(intersects[1].object.name).name + ".png");
                  canvas.clearCanvas(c.width, c.height);
                  canvas.updateStatus();
                  var card = JSON.parse(intersects[1].object.name);
                  canvas.returnCanvas("", 0, 0, "Name: " + card.name, c.width / 2, 25, 20);
                  if (cardToPlay.type === "Weather" || card.type === "Horn") {
                    canvas.returnCanvas("", 0, 0, "Ability: " + card.ability, c.width / 2, 50, 20);
                    canvas.returnCanvas("", 0, 0, "Press ESC to close view.", c.width / 2, 75, 20);
                  } else {
                    canvas.returnCanvas("", 0, 0, "Type: " + card.type, c.width / 2, 50, 20);
                    for (var i = 0; i < card.ability.split("|").length; i++) {
                      canvas.returnCanvas("", 0, 0, "Ability: " + card.ability.split("|")[i], c.width / 2, 75 + 25 * i, 15);
                    }
                    canvas.returnCanvas("", 0, 0, "Strength: " + card.strength, c.width / 2, 150, 20);
                    canvas.returnCanvas("", 0, 0, "Press ESC to close view.", c.width / 2, 175, 20);
                  }
                  break;
				case "board":
					return;
                default:
                  break;
              }
            }
              loader.load(
                'json/card.json',
                function(geometry) {
                  var material = new THREE.MeshBasicMaterial({
                    map: texture,
                    side: THREE.DoubleSide
                  });
                  var karta = new THREE.Mesh(geometry, material);
                  karta.position.y = 0.5;
                  karta.position.x = 1.5;
                  karta.position.z = 1.5;
                  karta.scale.set(7, 1, 7);
                  karta.name = "displayed_card";
                  scene.add(karta);
                });
              return;
          }
          if (playerTurn) {
            try {
              console.log(JSON.parse(intersects[0].object.name));
              canvas.clearCanvas(c.width, c.height);
              canvas.updateStatus();
              cardToPlay = JSON.parse(intersects[0].object.name);
              canvas.returnCanvas("", 0, 0, "Name: " + cardToPlay.name, c.width / 2, 25, 20);
              if (cardToPlay.type === "Weather" || cardToPlay.type === "Horn") {
                gui.removeLight();
                gui.addLight(cardToPlay.name);
                scene.getChildByName("board").material.needsUpdate = true;
                canvas.returnCanvas("", 0, 0, "Ability: " + cardToPlay.ability, c.width / 2, 50, 20);
                canvas.returnCanvas("", 0, 0, "Press ESC to resign or ENTER to play this card.", c.width / 2, 75, 20);
              } else {
                gui.removeLight();
                gui.addLight(cardToPlay.type);
                scene.getChildByName("board").material.needsUpdate = true;
                canvas.returnCanvas("", 0, 0, "Type: " + cardToPlay.type, c.width / 2, 50, 20);
                for (var i = 0; i < cardToPlay.ability.split("|").length; i++) {
                  canvas.returnCanvas("", 0, 0, "Ability: " + cardToPlay.ability.split("|")[i], c.width / 2, 75 + 25 * i, 15);
                }
                canvas.returnCanvas("", 0, 0, "Strength: " + cardToPlay.strength, c.width / 2, 150, 20);
                canvas.returnCanvas("", 0, 0, "Press ESC to resign or ENTER to play this card.", c.width / 2, 175, 20);
              }
            } catch (e) {
              console.log(e);
              console.log(intersects[0].object.name);
            }
          } else {
            return;
          }
        }
      }
    }
  }
}
