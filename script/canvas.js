var c, ctx;
/*-----------------------Players stats--------------------------*/
var enemyDeck, enemyHandLength, enemyLife, enemyScore, enemySurrender;
var playerHandLength, playerLife, playerScore, playerSurrender;

function Canvas() {

  function init() { //deklaracja canvasa
    c = document.createElement("canvas");
    c.width = innerWidth; // bez px;
    c.height = 200; // bez px;
    c.id = "menu";
    ctx = c.getContext("2d");
    document.body.appendChild(c);
  }

  this.returnCanvas = function(image, imageX, imageY, text, textX, textY, textSize) { //wrucanie tresci do canvasa
    if (image !== "") {
      var img = new Image();
      img.onload = function() {
        ctx.drawImage(img, imageX, imageY);
      };
      img.src = image;
    }
    if (text !== "") {
      ctx.font = textSize + "px dragonslapper";
      ctx.fillStyle = "#de833a";
      ctx.textAlign = "center";
      ctx.fillText(text, textX, textY);
    }
  };

  this.showTurn = function() {
    if (playerTurn) {
      canvas.returnCanvas("buttons/status/you.png", innerWidth / 2 - 100, 0, "", 0, 0, 0);
      canvas.returnCanvas("", 0, 0, "Your turn", innerWidth / 2 + 200, 100, 25);
    } else {
      canvas.returnCanvas("buttons/status/enemy.png", innerWidth / 2 - 100, 0, "", 0, 0, 0);
      canvas.returnCanvas("", 0, 0, "Enemy turn", innerWidth / 2 + 200, 100, 25);
    }
  };

  this.updateStatus = function() { //aktualizowanie stanu playera i przeciwnika | BRAKUJE PODDANSTWA
    canvas.returnCanvas("", 0, 0, "You:", 55, 40, 25);
    canvas.returnCanvas("", 0, 0, "Deck: " + currentDeck.name, 90, 70, 15);
    canvas.returnCanvas("buttons/status/card.png", 100, 10, "", 0, 0, 0);
    canvas.returnCanvas("", 0, 0, handDeck.length, 150, 40, 20);
    if (playerLife == 2) {
      canvas.returnCanvas("buttons/status/fullLife.png", 170, 10, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/fullLife.png", 215, 10, "", 0, 0, 0);
    } else if (playerLife == 1) {
      canvas.returnCanvas("buttons/status/brokenLife.png", 170, 10, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/fullLife.png", 215, 10, "", 0, 0, 0);
    } else {
      canvas.returnCanvas("buttons/status/brokenLife.png", 170, 10, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/brokenLife.png", 215, 10, "", 0, 0, 0);
    }
    canvas.returnCanvas("", 0, 0, "Score: " + playerScore, 310, 40, 20);
    if (playerSurrender) {
      canvas.returnCanvas("buttons/surrender.png", 370, 10, "", 0, 0, 0);
    }

    canvas.returnCanvas("", 0, 0, "Enemy:", 55, 140, 25);
    canvas.returnCanvas("", 0, 0, "Deck: " + enemyDeck, 90, 170, 15);
    canvas.returnCanvas("buttons/status/card.png", 100, 110, "", 0, 0, 0);
    canvas.returnCanvas("", 0, 0, enemyHandLength, 150, 140, 20);
    if (enemyLife == 2) {
      canvas.returnCanvas("buttons/status/fullLife.png", 170, 110, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/fullLife.png", 215, 110, "", 0, 0, 0);
    } else if (enemyLife == 1) {
      canvas.returnCanvas("buttons/status/brokenLife.png", 170, 110, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/fullLife.png", 215, 110, "", 0, 0, 0);
    } else {
      canvas.returnCanvas("buttons/status/brokenLife.png", 170, 110, "", 0, 0, 0);
      canvas.returnCanvas("buttons/status/brokenLife.png", 215, 110, "", 0, 0, 0);
    }
    canvas.returnCanvas("", 0, 0, "Score: " + enemyScore, 310, 140, 20);
    if (enemySurrender) {
      canvas.returnCanvas("buttons/surrender.png", 370, 110, "", 0, 0, 0);
    }
  };

  this.clearCanvas = function(width, height) { //czyszczenie canvasa
    ctx.fillStyle = "#2f2f2f";
    ctx.fillRect(0, 0, width, height);
  };

  init();
}
